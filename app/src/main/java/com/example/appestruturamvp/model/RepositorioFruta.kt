package com.example.appestruturamvp.model

class RepositorioFruta {

    private var frutas = arrayListOf<String>(
        "maça",
        "laranja",
        "limao",
        "abacati"
    )

    fun getFrutas(): ArrayList<String>{
        return frutas
    }

}