package com.example.appestruturamvp.ui.main

interface MainContrato {

    interface View{

        fun exibirMensagem(mensagem: String)

        fun redirecionarTela()

    }

    interface Presenter{

        fun login(email:String, senha:String)

    }

}