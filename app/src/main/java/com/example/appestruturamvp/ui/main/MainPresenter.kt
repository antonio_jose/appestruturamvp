package com.example.appestruturamvp.ui.main

import android.view.View
import com.example.appestruturamvp.model.RepositorioSessao

class MainPresenter(private val view: MainContrato.View) : MainContrato.Presenter {

    var repositorioSessao: RepositorioSessao = RepositorioSessao()

    override fun login(email: String, senha: String) {

        if(email.isEmpty() || senha.isEmpty()){
            view.exibirMensagem("Existem campos vázios")
        }else{

            var resultado = repositorioSessao.login( email, senha )

            if(resultado){
                view.exibirMensagem("Login efetuado com sucesso")
                view.redirecionarTela()
            }else{
                view.exibirMensagem("Erro ao tentar logar")
            }

        }

    }


}