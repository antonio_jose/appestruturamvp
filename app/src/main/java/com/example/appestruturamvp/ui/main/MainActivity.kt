package com.example.appestruturamvp.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.appestruturamvp.R
import com.example.appestruturamvp.ui.fruta.FrutaActivity


class MainActivity : AppCompatActivity(), MainContrato.View{

    lateinit var mainPresenter: MainPresenter

    lateinit var edit_email: EditText
    lateinit var edit_senha: EditText
    lateinit var butto_entrar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainPresenter = MainPresenter( this )

        edit_email = findViewById(R.id.edit_email)
        edit_senha = findViewById(R.id.edit_senha)
        butto_entrar = findViewById(R.id.button_entrar)

        butto_entrar.setOnClickListener {

            var email = edit_email.text.toString()
            var senha = edit_senha.text.toString()

            mainPresenter.login( email,senha )
        }




    }

    override fun exibirMensagem(mensagem: String) {

        Toast.makeText(this, mensagem, Toast.LENGTH_SHORT).show()

    }


    override fun redirecionarTela() {
        startActivity(Intent(this, FrutaActivity::class.java))
    }
}