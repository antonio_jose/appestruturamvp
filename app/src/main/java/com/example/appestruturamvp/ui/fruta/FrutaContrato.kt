package com.example.appestruturamvp.ui.fruta

interface FrutaContrato {

    interface View{

        fun mostrarDados(list: ArrayList<String>)

        fun redicecionarTela()

    }

    interface Presenter{

        fun getFrutas()

    }

}