package com.example.appestruturamvp.ui.fruta

import com.example.appestruturamvp.model.RepositorioFruta

class FrutaPresenter(private val view: FrutaContrato.View) : FrutaContrato.Presenter {

    private var repositorioFruta: RepositorioFruta = RepositorioFruta()

    override fun getFrutas() {

        var list = repositorioFruta.getFrutas()
        var list1 = ArrayList<String>()

        for (i in list){

                list1.add( i )

        }

        view.mostrarDados( list1 )


    }

    fun redirecionarTela(){

        view.redicecionarTela()

    }
}