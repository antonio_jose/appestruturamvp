package com.example.appestruturamvp.ui.fruta

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import com.example.appestruturamvp.R

class FrutaActivity : AppCompatActivity(), FrutaContrato.View {

    lateinit var frutaPresenter: FrutaPresenter
    lateinit var listView: ListView
    lateinit var arrayAdapter: ArrayAdapter<String>
    lateinit var listFrutas: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fruta)

        frutaPresenter = FrutaPresenter(this)


        listView = findViewById(R.id.listView_frutas)
        frutaPresenter.getFrutas()


        listView.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(this, "Item "+listFrutas[position],Toast.LENGTH_SHORT).show()
        }



    }

    override fun mostrarDados(list: ArrayList<String>) {
        listFrutas = list
        arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, list)
        listView.adapter = arrayAdapter

    }

    override fun redicecionarTela() {

    }
}